-- Active: 1673947616242@@127.0.0.1@3306

DROP TABLE IF EXISTS articles_tag;
DROP TABLE IF EXISTS commentaire;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS articles;

CREATE TABLE articles(
    id INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(100),
    image VARCHAR(255),
    contenu TEXT,
    date DATE,
    nombreDeVue INT
);

CREATE TABLE commentaire(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user VARCHAR(20),
    contenu VARCHAR(255),
    date DATE,
    email VARCHAR(100),
    idArticles INT,
    Foreign Key (idArticles) REFERENCES articles(id) ON DELETE CASCADE
);

CREATE TABLE tag(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(20)
);


CREATE TABLE articles_tag(
    articles_id INT,
    tag_id INT,
    PRIMARY KEY (articles_id , tag_id),
    Foreign Key (articles_id ) REFERENCES articles(id) ON DELETE CASCADE,
    Foreign Key (tag_id) REFERENCES tag(id) ON DELETE CASCADE
); 

INSERT INTO articles (titre,image,contenu,date,nombreDeVue) VALUES
('Le Roi Arthur',
'https://cdna.artstation.com/p/assets/images/images/056/923/762/large/alexandre-hildans-sabre-foi.jpg?1670415274',
"Saber (セイバー, Seibā?) est la première héroïne principale de Fate/stay night et le Servant d'Emiya Shirou durant la 5e Guerre du Saint Graal. Elle est le Servant d'Emiya Kiritsugu durant la 4e Guerre du Saint Graal de Fate/Zero.
La véritable identité de Saber est celle d'Artoria Pendragon (アルトリア・ペンドラゴン, Arutoria Pendoragon?, Arturia ou Altria), mieux connue sous le nom d'Arthur Pendragon (アーサー・ペンドラゴン, Āsā Pendoragon?) et du Roi Arthur (アーサー王, Āsā-Ō?), qui est connu en tant qu'homme dans l'histoire. Elle est considérée comme un héros légendaire de la Grande-Bretagne connue en tant que Roi des Chevaliers. Elle est celle qui a retirée Caliburn (L'épée de la Sélection) du rocher, qui sera détruite plus tard. Elle obtient ultérieurement Excalibur et Avalon de la part de la Dame du Lac, la Fée Viviane.
Bien qu'Arthur soit un héros bien connu pour être un modèle pour les chevaliers, il est dit qu'il a réellement existé comme 'Dux Bellorum', le guide de la Guerre, et était un grand général qui a dirigé les douze rois du nord de l'Angleterre, Gododdin, repoussant l'invasion de forces étrangères comme les écossaises et pictes. Cette véritable identité du grand chef derrière la légende du Roi Arthur a beaucoup de variations et on pense qu'il y aurait pu y avoir deux personnes qui correspondent à la description du roi. L'un est l'Arthur Britannique, tandis que l'autre est l'Artorius romain et leurs deux ensembles distincts d'accomplissements sont censés avoir fusionnés dans la légende du roi Arthur qui est connue de nos jours.
Enfance
Artoria est née dans un temps de chaos et de guerre qui a débuté avec la disparition de l'empire romain. On croyait qu'il était indestructible, mais on n'attendait que sa destruction par les invasions barbares. En préparation de la guerre contre les barbares, Rome a privé sa province de l'île de toute force militaire qui y été restée. Une fois que Britannia a perdu la protection de l'empire, elle ne pouvait pas échapper au fait de devenir indépendante, ce qui l'entraînait à éclater en plusieurs pays plus petits peu de temps après. Ces temps d'invasions barbares et de conflits autodestructeurs entre clans avait débuté une longue période de guerre qui deviendra plus tard 'les Âges sombres'. Artoria est née dans cette période comme l'héritier du trône, la fille du roi de Grande-Bretagne, Uther Pendragon, et de Ygraine, la femme de Gorlois, le Duc de Cornouailles. Le roi, croyant à la prophétie de Merlin, a aspiré à la naissance du successeur désigné, mais l'enfant né n'était pas celui qu'il désirait.

Il est dit dans la légende que Arthur a été laissé à Merlin comme le prix de son aide pour assurer le succès de l'amour d'Uther pour Ygraine, disant: « Je vais guider cet enfant correctement, l'un portant un grand destin et le protéger de la crise de la famille royale. » En réalité Artoria n'était pas un garçon, donc le roi ne pouvait pas faire d'un enfant qui n'était pas un homme son successeur, même s'il était de son destin de le devenir un jour. Elle a été confiée aux vassaux du roi et devait être élevée comme l'enfant d'un simple chevalier. Le roi est tombé dans le désespoir face à la situation, mais Merlin était ravi parce que le sexe de celui qui devait devenir roi n'avait jamais été très important. Il était convaincu que le fait que la fille était séparée du château jusqu'au jour de la prophétie était la preuve qu'elle deviendrait roi.

Merlin a placé Artoria sous les soins de Sir Hector, pun chevalier simple et sage, qui l'a élevée comme son propre enfant. Bien que Hector n'ait pas cru en la prophétie, il ressentait le même air qu'il ressentait de son roi en la fille. Il a estimé qu'il devait l'élever comme un chevalier, et il souhaitait qu'elle se développe. Il n'a jamais eu besoin de souhaiter une telle chose, car elle s'est formée jour après jour pour devenir plus forte que quiconque. Elle a juré de porter son épée pour la seule raison que 'seul un roi peut sauver un pays en ruine destiné à tomber' sans qu'on ait besoin de lui dire. Pendant ce temps, elle a été élevée avec Sir Kay, comme son frère, mais ils ont continué à toujours s'aimer comme frères et sœurs même après avoir appris la vérité derrière leur relation. Elle a agi comme son écuyer et a reçu une formation de sa part, tout en faisant d'autres corvées telles que tirer son cheval. Elle était meilleure que lui en termes d'épéisme, mais elle ne l'a jamais battu en raison de ses arguments le déclarant vainqueur plutôt qu'à cause de compétences réelles.


Artoria s’apprêtant à tirer l'épée du rocher

Une fois que le jour de la prophétie était arrivé, les chevaliers et les seigneurs de tout le pays se sont rassemblés pour être choisis comme roi. Chacun s'attendait à ce que la sélection soit fait par la joute pour sélectionner le plus supérieur qui pourrait devenir un roi, mais la seule chose préparée au lieu de cette sélection était une épée enfoncée dans une pierre avec une inscription dorée sur la poignée disant 'Quiconque retirera cette épée de la pierre sera proclamé Roi d'Angleterre'. Alors que de nombreux chevaliers ont saisi l'épée en essayant de suivre la commande, aucun n'a été capable de l'enlever. Alors qu'ils commencèrent la méthode de sélection prévue en joutant, Artoria, le seul apprenti non qualifié pour les joutes, s'approcha de la pierre déserte de la sélection et se tendit vers l'épée sans hésiter.

Avant de l'attraper, Merlin apparut devant elle pour lui dire de réfléchir avant de l'enlever. Il lui dit qu'elle ne serait plus humaine lorsque qu'elle retirera l'épée, qu'elle subira la haine de certains humains et que cette voie la conduira à une horrible mort, mais elle a tout simplement répondu que dans ces visions les gens souriaient et que cette voie n'étais sûrement pas la mauvaise. Elle avait été préparée au fait que 'devenir un roi est signe de renoncer à son humanité' depuis sa naissance elle savait qu'un roi était quelqu'un qui devait sacrifier des choses pour protéger les autres. Elle y pensait tous les soirs et frémissait jusqu'à ce que le soleil se lève. Même si aucun jour n'est passé sans qu'elle ne craigne ce moment, elle savait que ce jour viendrait et qu'elle devra être prête. Elle retira l'épée comme si c'était naturel de pouvoirs le faire, et la zone était sublimée de lumière. Elle était devenue quelque chose qui n'était pas humain en cet instant. Pour le peuple, le sexe du roi n'a pas d'importance, et personne ne se préoccupera de l'apparence du roi tant qu'il se comportera et agira comme tel. Même si quelqu'un aurait remarqué que le roi était une femme, ce ne serait pas un problème s'il était un bon roi. Elle a arrêté de vieillir à ses 15 ans, après qu'elle ai retirée Caliburn de la pierre.

Règne

Artoria aux côtés de Bédivère (droite), Gauvain (plus à droite) et de Lancelot (gauche)

Artoria a mené la Grande-Bretagne de Camelot, et après être devenu un seigneur féodal comme son père, elle est devenue un roi avec beaucoup de chevaliers sous ses ordres, y compris les estimés Chevaliers de la Table Ronde. Suite à la bénédiction qui lui a été accordée, de nombreux chevaliers l'ont crainte comme sinistre. La plupart d'entre eux a loué l'immortalité de leur maître comme divine. Ses batailles par la suite étaient les actes d'un dieu de la guerre. Elle a toujours conduit son armée de l'avant, et aucun ennemi ne pouvait s'opposer à elle. Il n'y avait pas de défaite pour un corps admiré comme un dragon sous forme humaine. Elle a connu seulement la victoire pendant dix ans et douze batailles alors qu'elle vivait ces jours comme un roi. Elle n'a jamais reculé et n'a jamais été déshonorée. Elle a été élevée en tant que roi et a rempli ses obligations en tant que roi.

Affaires personnelles
En raison du problème consistant à devoir reproduire un héritier, Merlin a utilisé sa magie pour la transformer en un pseudo-homme capable de produire du sperme pour une durée de temps inconnue. Pendant cette période, elle était ensorcelée par sa sœur, La Fée Morgane, qui a pris le sperme d'Artoria, l'a développé dans ses propres ovaires, a donné naissance à Homonculus qui était le clone complet d'Artoria, Mordred. Elle est née et a grandi sans connaissances d'Artoria, grandissant rapidement en raison de son vieillissement accéléré dû au fait que c'étais un Homonculus. Elle a réussi à rejoindre la Table Ronde grâce à ses propres efforts et à la recommandation de Morgane. Elle a adoré son roi en cachant son identité, et elle était extatique d'apprendre son réel héritage.

En revendiquant son héritage et le droit au trône, Artoria a complètement rejeté Mordred et a refusé de la reconnaître comme son héritier. Mordred a cru que la raison pour laquelle elle n'était pas acceptée était due à la haine que son père portait à Morgane, et que, peu importe combien elle travaillait pour s'exceller par rapport aux autres, elle serait toujours considérée comme une existence sale. L'amour et l'admiration qu'elle avait pour le roi était si grand que le rejet d'Artoria transforma cette admiration en haine profonde.

Artoria a épousé Guenièvre, mais ce n'était que par obligation d'avoir une femme. Une des vérités était que Lancelot, l'un des amis les plus proches d'Artoria, et Guenièvre s'aimaient, et comme Artoria devait cachée son sexe, Guenièvre devait porter ce fardeau toute sa vie. Guenièvre a ressenti une culpabilité pour ses actions, mais a senti un soulagement quand Lancelot était tombé amoureux d'elle. Lui, qui a partagé les mêmes idéaux que son roi, partage le fardeau avec elle et ne ferait jamais tomber le pays dans une situation dangereuse.

Royaume
Artoria devait agir comme le fils du roi, car celui qui gouvernait les nombreux territoires et contrôlait les chevaliers devait être un homme. Alors que certaines personnes se sont méfiées d'elle, seul son père, Merlin et Kay connaissaient la vérité exacte sur son identité. Elle s'est littéralement couverte d'acier pour cacher cette vérité toute sa vie et, en raison de la protection des fées, personne ne se questionnait à propos de son petit corps ou de son visage qui semblait être celui d'une fille. Elle a été considéré par les chevaliers comme un beau roi, un guerrier invincible dont l'apparence ou la taille du corps ont eu un impact sur sa position.

Pendant ce temps, les personnes vivant avec la peur des invasions sauvages voulaient un roi fort, et les chevaliers ne suivraient qu'un excellent commandant peu importe son sexe ou son physique. Après avoir satisfait ces critères, personne ne l'a remis en question. Elle était considérée comme juste et courageuse car elle se tenait toujours en tête de l'armée, battant ses ennemis sur le champ de bataille. Alors que de nombreux ennemis et civils sont morts, les choix du roi ont toujours été considérés comme corrects, et elle a été meilleur roi que quiconque. Personne ne doutait tant que le roi avait raison.

Son armée a reconstruit la cavalerie perdue, et ne connaissant aucune perte, elle a traversé le champ de bataille tout en battant les infanteries étrangères et traversant de nombreux remparts. Beaucoup de personnes ont été écartées car elle se joignait aux batailles, et tous ses ennemis devaient être vaincus une fois qu'elle aurait rejoint le combat. C'était une pratique courante pour les militaires de répondre à ses besoins en pillant toutes les ressources d'un village local pour fournir une aide à la bataille afin de protéger le pays. On peut dire qu'aucun chevalier n'a tué plus de gens qu'elle, et on ne sait pas si elle a considéré cela comme un fardeau.

Elle a strictement tenu le serment qui disait qu'un roi n'est pas humain et que ce dernier ne peut protéger les personnes avec des émotions humaines. Elle n'a jamais plissé ses yeux de douleur étant assise sur le trône, et elle a réglé tous les problèmes en travaillant fortement dans les affaires du gouvernement. Elle a réussi à équilibrer le pays sans aucune déviation, et elle a puni les individus sans une seule faille. Même après, ou peut-être à cause du fait d'avoir remporté des batailles, commandé des citoyens sans désordre et puni des centaines de criminels, un de ses chevaliers a murmuré: « Le roi Arthur ne comprend pas les sentiments humains ».

Il est possible que tout le monde se sentait ainsi, car plus elle devenait parfaite en tant que roi, plus ils devaient la considérer comme souverain. Ils ont estimé qu'un humain sans émotions ne pouvait pas régner sur les autres, ce qui conduit à plusieurs chevaliers réputés de quitter Camelot. Elle a tout simplement accepté cela comme un événement naturel qui fait partie du processus de gouvernement, isolant le juste roi honoré par ses chevaliers. Ayant abandonné ses émotions depuis le début, elle n'a pas changé d'avis même si elle avait été abandonnée, crainte ou encore trahie. Il n'y avait pas de bon ou de mauvais pour quelqu'un qui avait considéré de tels événements comme insignifiants.

Sa bataille finale pour son pays a commencé d'une telle manière, et s'est achevée par une victoire complète. Les envahisseurs ont cherché la réconciliation en raison de ses résultats écrasants, et son pays qui attendait seulement la destruction a gagné une brève période de paix. Le pays a finalement commencé à redevenir la terre dont elle avait rêvé.

Chute
L'affaire Lancelot avec Guenièvre a finalement été révélée par les plans de traîtres qui détestaient Camelot, les amenant à s'opposer. Artoria n'a pas vu cette action inéluctablement injuste comme une trahison, mais a plutôt compris le sacrifice de Guenièvre en raison de la dissimulation de son sexe réel. Elle a tout de même agit comme un roi et Guenièvre devait être exécutée pour trahison. Lancelot ne pouvait pas laisser Guenièvre être exécutée, Il est alors venu empêcher son exécution tuant au passage de nombreux chevaliers ainsi que Gaheris et Gareth qui ont tentés de s'interposés puis blessa profondément Gauvain avant de partir en exil en France dans son domaine, ce qui provoqua un drame au sein de Camelot ou personne n'avait tort ni raison. Malgré tous ces événements Artoria écrivit une lettre de pardon à Lancelot, en donnant ces bénédictions à lui et à Guenièvre, disant que si Lancelot c'est engagé dans de telles actions c'est qu'elles 'doivent avoir une bonne cause'.

Le dernier affrontement d'Arthur et Mordred vu par TYPE-MOON (Le dernier affrontement d'Arthur et Mordred)

La fourreau d'Excalibur a été volé pendant qu'elle livrait des batailles en dehors de son pays; Une fois retournée à l'intérieur de son pays après avoir remportée ces batailles et formée un traité avec Rome, elle a découvert que la Grande-Bretagne était déchirée par les troubles civils. Malgré ses efforts courageux pour apaiser la dissidence elle n'a pas eu le choix que d'affronter la colère et les troupes ennemis qui ont attendu le retour de l'armée d'Artoria pour les attaquer en embuscade. Elle a été mortellement blessée par le Chevalier de la Trahison, Mordred lors de la Bataille de Camlann. Une fois la bataille terminée elle se blâma sur ses échecs, regrettant sa vie de roi et ces actions en tant que tel. Avant son dernier souffle, elle fit un contrat avec le monde. En échange de ses services, elle demanda à recevoir l'occasion d'obtenir le Saint Graal pour sauver son pays de la destruction.

Apparence
Saber est une belle et attirante jeune femme d'un mètre 54 qui pèse 42 kg. Elle a généralement les yeux d'un vert tel qu'il inspire loyauté, dureté et fermeté. Ses longs cheveux blonds sont attachés en un chignon surplombé d'une tresse. Un nœud bleu maintient l'ensemble. Elle possède une armure dont le tissu est d'un bleu roi. Des motifs bleus parsèment l'armure d'acier, qui se finit par de la dentelle elle aussi parsemée de joyaux jaunes.

Elle possède aussi une tenue de tous les jours. Sa tenue est composée d'un chemisier blanc dont le col est serré avec un nœud bleu, d'une jupe portée très haute, juste en dessous la poitrine et lui arrivant aux genoux. Ses chaussures sont de simples bottines en cuir. 


",
'2023-07-02','69420'),

("Queen's Gambit",
'https://cdna.artstation.com/p/assets/images/images/039/280/968/large/alexandre-hildans-queen-s-gambit.jpg?1625475848',
"Elizabeth Harmon est un personnage fictif et le principal protagoniste du roman de Walter Tevis The Queen's Gambit",
'2021-01-01','1'),

("Arwen Undomiel",
'https://cdnb.artstation.com/p/assets/images/images/039/280/829/large/alexandre-hildans-arwen-oreilles-pointues.jpg?1625475517',
"Elle a les oreilles pointues",
"241-01-01","0")

;

INSERT INTO commentaire (user,contenu,date,email,idArticles) VALUES
('Jean',"J'aime les Corgis", '2023-07-02','jeandemal@simplon.fr',1 ),
('Rama',"J'aime pas les Clown", '2023-07-02','ramaaimelechocolat@simplon.fr',2);

INSERT INTO tag (title) VALUES
('Digital'),('Traditional');

INSERT INTO articles_tag (articles_id,tag_id) VALUES
(1,1),(1,2),
(2,1);