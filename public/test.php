<?php


// use App\Entities\Articles;
use App\Entities\Articles;
use App\Entities\Commentaire;
use App\Entities\Tag;
use App\Repository\ArticlesRepository;
use App\Repository\CommentaireRepository;
use App\Repository\CR;
use App\Repository\Database;
use App\Repository\TagRepository;


use App\Kernel;
require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};

// require('../vendor/autoload.php');

// $connection = Database::connect();

// $commments = new CommentaireRepository;

// $com = new Commentaire('User', 'Contenu', new DateTime(), 'mail', 1);
// //$commments->persist($com);

// $commments->delete(3);

// $commments->update(
//     new Commentaire(
//         'Erdnaxela',
//         "Php c'est un vieux language et du coup il faut bien faire attention avec les espaces et les ; <br> Ah ! Et aussi faites attentions, PHP aime l'argent",
//         new DateTime(),
//         'pouet@mail.xyz',
//         1,
//         4
//     )
// );

// var_dump($commments->findByUser("Jean"));




// echo 'Affiche tous les commentaires';
// var_dump($commments->findAll());

// echo 'Affiche les commentaires avec une id donné ou null';
// var_dump($commments->findById(2));
// var_dump($commments->findById(212));



// $articles = new ArticlesRepository;

// //Ajoute l'article à la BDD
// $tabelo = new Articles("Titre", "url de l'image", "contenu", new DateTime(), 0);
// //$articles->persist($tabelo);
// $articles->delete(3);

// //Met à jour un article en lui donnant un article en fonction de l'id donnée, ici 4
// $articles->update(new Articles("update", "url de l'image", "maj", new DateTime(), -1230,4));


// echo 'Affiche tous les articles en fonction de leur nombre de vue';
// var_dump($articles->findAllOrderByView());

// echo 'Affiche tous les articles';
// var_dump($articles->findAll());

// echo 'Affiche les articles avec une id donné ou null';
// var_dump($articles->findById(1));
// var_dump($articles->findById(12));



// $tag = new TagRepository;

// $nb = new Tag("Noir et Blanc");
// //Ajoute le tag à la BDD
// //$tag->persist($nb);

// //Suprrime le tag 4
// //$tag->delete(4);

// //Met à jour le nom du tag avec un id et un titre
// $tag->update(5, "Charcoal");

// echo 'Affiche tous les tags';
// var_dump($tag->findAll());

// echo 'Affiche le tag avec une id donné ou null';
// var_dump($tag->findById(1));
// var_dump($tag->findById(12));

// echo 'Affiche tous les tag qui sont lié à un Article';
// var_dump($tag->findByArticles(1));

// echo 'Affiche tous les articles qui ont le tag Digital';
// var_dump($tag->findByTag($tag->findById(1)));
