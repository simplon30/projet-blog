<?php

namespace App\Repository;
use App\Entities\Articles;
use PDO;

class ArticlesRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    public function findAllOrderByView(){
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM articles ORDER BY NombreDeVue DESC');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $date = new \DateTime($key['date']);
            $articles[] = new Articles($key['titre'], $key['image'], $key['contenu'], $date, $key['nombreDeVue'],$key['id']);
        }

        return $articles;
    }

    public function findAll(){
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM articles');
        $statement->execute();
        $result = $statement->fetchAll();

        if($result){

            foreach ($result as $key) {
                $date = new \DateTime($key['date']);
                $articles[] = new Articles($key['titre'], $key['image'], $key['contenu'], $date, $key['nombreDeVue'],$key['id']);
            }
            
            return $articles;
        }
        return null;
    }

    public function findById(int $id){
        $statement = $this->connection->prepare('SELECT * FROM articles WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();

        if($result) {
            $date = new \DateTime($result['date']);
            return  new Articles($result['titre'], $result['image'], $result['contenu'], $date, $result['nombreDeVue'],$result['id']);
        }

        return null;
    }

    public function persist(Articles $articles) {
        $statement = $this->connection->prepare('INSERT INTO articles(titre,image,contenu,`date`,`nombreDeVue`) VALUES ( :titre, :image, :contenu, :date, :nombreDeVue )');
        $statement->bindValue('titre', $articles->getTitre());
        $statement->bindValue('image', $articles->getImage());
        $statement->bindValue('contenu', $articles->getContenu());
        $statement->bindValue('date', $articles->getDate()->format('Y-m-d'));
        $statement->bindValue('nombreDeVue', $articles->getNombreDeVue());

        $statement->execute();
        $articles->setId($this->connection->lastInsertId());
    }

    public function delete(Articles $articles){
        $statement = $this->connection->prepare('DELETE FROM articles WHERE id = :id');
        $statement->bindValue('id', $articles->getId());
        $statement->execute();
    }

    public function update(Articles $articles) {
        $statement = $this->connection->prepare('UPDATE articles SET titre = :titre, image = :image, contenu = :contenu, date = :date, nombreDeVue = :nombreDeVue WHERE id = :id');
        $statement->bindValue('titre', $articles->getTitre());
        $statement->bindValue('image', $articles->getImage());
        $statement->bindValue('contenu', $articles->getContenu());
        $statement->bindValue('date', $articles->getDate()->format('Y-m-d'));
        $statement->bindValue('nombreDeVue', $articles->getNombreDeVue());
        $statement->bindValue('id', $articles->getId(), PDO::PARAM_INT);
        
        $statement->execute();
    }

    public function ndv(Articles $articles) {
        $statement = $this->connection->prepare('UPDATE articles SET nombreDeVue = :nombreDeVue WHERE id = :id');
        $statement->bindValue('nombreDeVue', $articles->getNombreDeVue()+1);
        $statement->bindValue('id', $articles->getId(), PDO::PARAM_INT);
        
        $statement->execute();
    }



}

