<?php

namespace App\Repository;

use App\Entities\Tag;
use PDO;

class TagRepository{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }
    /**
     * Affiche tout les articles en fonction d'un tag
     * @param Tag $tag
     * @return array
     */
    public function findByTag(Tag $tag){
        $statement = $this->connection->prepare('SELECT titre,title
        FROM articles_tag 
        JOIN articles a ON a.id=articles_id
        JOIN tag t ON t.id=tag_id
        WHERE tag_id = :tagid');

        $statement->bindValue('tagid', $tag->getId());
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    /**
     * Affiche tout les tag en fonction d'un article
     * @param int $idArticles
     * @return array
     */
    public function findByArticles(int $idArticles){
        $statement = $this->connection->prepare('SELECT titre,title
        FROM articles_tag 
        JOIN articles a ON a.id=articles_id
        JOIN tag t ON t.id=tag_id
        WHERE articles_id = :idArticle');

        $statement->bindValue('idArticle',$idArticles);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
    /**
     * Affiche tout les tags
     * @return array<Tag>
     */
    public function findAll(){
        $tag = [];
        $statement = $this->connection->prepare('SELECT * FROM tag');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $tag[] = new Tag($key['title'], $key['id']);
        }

        return $tag;
    }

    /**
     * Affiche un tag en fonction de l'id ou return null si null ^^
     * @param int $id
     * @return Tag| Null
     */
    public function findById(int $id) : Tag | Null{
        $statement = $this->connection->prepare('SELECT * FROM tag WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();

        if($result) {
            return new Tag($result['title'], $result['id']);
        }

        return null;

    }

    /**
     * Ajoute un tag
     * @param Tag $tag
     * @return void
     */
    public function persist(Tag $tag) {
        $statement = $this->connection->prepare('INSERT INTO tag (title) VALUES ( :title )');
        $statement->bindValue('title', $tag->getTitle());
        $statement->execute();
        $tag->setId($this->connection->lastInsertId());
    }

    /**
     * Supprime un tag en lui donnant sont id
     * @param int $id
     * @return void
     */
    public function delete(Tag $tag){
        $statement = $this->connection->prepare('DELETE FROM tag WHERE id = :id');
        $statement->bindValue('id', $tag->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    /**
     * Met à jour un tag en lui donnant l'id et un nouveau titre
     * @param int $id
     * @param string $title
     * @return void
     */
    public function update(Tag $tag) {
        $statement = $this->connection->prepare('UPDATE tag SET title = :title WHERE id = :id');
        $statement->bindValue('id', $tag->getId());
        $statement->bindValue('title', $tag->getTitle());
        $statement->execute();
    }

}