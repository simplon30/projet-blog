<?php

namespace App\Repository;

use App\Entities\Commentaire;
use App\Repository\Database;
use PDO;

class CommentaireRepository{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM commentaire');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $date = new \DateTime($key['date']);
            $articles[] = new Commentaire($key['user'], $key['contenu'], $date, $key['email'],$key['idArticles'],$key['id']);
        }

        return $articles;
    }

    public function findById(int $id){
        $statement = $this->connection->prepare('SELECT * FROM commentaire WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();

        if($result) {
            $date = new \DateTime($result['date']);
            return new Commentaire($result['user'], $result['contenu'], $date, $result['email'],$result['idArticles'],$result['id']);
        }

        return null;
    }

    public function persist(Commentaire $commentaire) {
        $statement = $this->connection->prepare('INSERT INTO commentaire(user,contenu,date,email,idArticles) VALUES( :user, :contenu, :date , :email, :idArticles)');
        $statement->bindValue('user', $commentaire->getUser());
        $statement->bindValue('contenu', $commentaire->getContenu());
        $statement->bindValue('date', $commentaire->getDate()->format('Y-m-d'));
        $statement->bindValue('email', $commentaire->getEmail());
        $statement->bindValue('idArticles', $commentaire->getIdArticles());
        
        $statement->execute();
        $commentaire->setId($this->connection->lastInsertId());
    }

    public function delete(Commentaire $commentaire){
        $statement = $this->connection->prepare('DELETE FROM commentaire WHERE id = :id');
        $statement->bindValue('id', $commentaire->getId());
        $statement->execute();
    }

    public function update(Commentaire $commentaire) {
        $statement = $this->connection->prepare('UPDATE commentaire SET user = :user, contenu = :contenu, date = :date, email = :email, idArticles = :idArticles WHERE id = :id');
        $statement->bindValue('user', $commentaire->getUser());
        $statement->bindValue('contenu', $commentaire->getContenu());
        $statement->bindValue('date', $commentaire->getDate()->format('Y-m-d'));
        $statement->bindValue('email', $commentaire->getEmail());
        $statement->bindValue('idArticles', $commentaire->getIdArticles());
        $statement->bindValue('id', $commentaire->getId(), PDO::PARAM_INT);
        
        $statement->execute();
    }

    public function findByUser(string $user){
        $statement = $this->connection->prepare('SELECT * FROM commentaire WHERE user = :user');
        $statement->execute(["user" => $user]);
    }
}