<?php
namespace App\Controller;
use App\Entities\Articles;
use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/articles')]
class ArticlesController extends AbstractController{

    private ArticlesRepository $repo;

    public function __construct(ArticlesRepository $repo){
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all(){
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}',methods: 'GET')]
    public function one(int $id){

        $articles = $this->repo->findById($id);
        $this->repo->ndv($articles);

        if(!$articles){
            throw new NotFoundHttpException();
        }
    
        return $this->json($articles, Response::HTTP_CREATED);
    }

    #[Route(methods : 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {

        $articles = $serializer->deserialize($request->getContent(), Articles::class, 'json');

        $articles->setDate((new \DateTime));
        $this->repo->persist($articles);

        return $this->json($articles, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {  

        $articles = $this->repo->findById($id);
        
        if(!$articles){
            throw new NotFoundHttpException();
        }

        $toUpdate = $serializer->deserialize($request->getContent(), Articles::class, 'json');

        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $articles = $this->repo->findById($id);
        if(!$articles){
            throw new NotFoundHttpException();
        }

        $this->repo->delete($articles);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

}