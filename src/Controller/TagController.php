<?php

namespace App\Controller;

use App\Entities\Tag;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/tag')]
class TagController extends AbstractController{

    private TagRepository $repo;

    public function __construct(TagRepository $repo){
        $this->repo = $repo;
    }

    #[Route(methods:'GET')]
    public function all(){
        $tags = $this->repo->findAll();
        return $this->json($tags);
    }

    #[Route('/{id}',methods: 'GET')]
    public function one(int $id){
        
        $tag = $this->repo->findById($id);

        if(!$tag){
            throw new NotFoundHttpException();
        }
    
        return $this->json($tag, Response::HTTP_CREATED);
    }

    #[Route(methods : 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {

        $tag = $serializer->deserialize($request->getContent(), Tag::class, 'json');

        $this->repo->persist($tag);

        return $this->json($tag, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {  

        $tag = $this->repo->findById($id);
        
        if(!$tag){
            throw new NotFoundHttpException();
        }

        $toUpdate = $serializer->deserialize($request->getContent(), Tag::class, 'json');

        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $tag = $this->repo->findById($id);
        if(!$tag){
            throw new NotFoundHttpException();
        }

        $this->repo->delete($tag);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

}
