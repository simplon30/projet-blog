<?php

namespace App\Entities;

class Tag {
    private ?int $id;
    private ?string $title;

    /**
     * @param int|null $id
     * @param string|null $title
     */
    public function __construct(?string $title,?int $id = null) {
        $this->title = $title;
    	$this->id = $id;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
}