<?php

namespace App\Entities;
use DateTime;

class Articles{
    private ?int $id;
    private ?string $titre;
    private ?string $image;
    private ?string $contenu;
    private ?DateTime $date;
    private ?int $nombreDeVue; 
    

    /**
     * @param int|null $id
     * @param string|null $titre
     * @param string|null $image
     * @param string|null $contenu
     * @param DateTime|null $date
     * @param int|null $nombreDeVue
     */
    public function __construct(string $titre, ?string $image, ?string $contenu, ?DateTime $date, ?int $nombreDeVue, ?int $id = null) {
    	$this->id = $id;
    	$this->titre = $titre;
    	$this->image = $image;
    	$this->contenu = $contenu;
    	$this->date = $date;
    	$this->nombreDeVue = $nombreDeVue;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitre(): ?string {
		return $this->titre;
	}
	
	/**
	 * @param string|null $titre 
	 * @return self
	 */
	public function setTitre(?string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param string|null $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContenu(): ?string {
		return $this->contenu;
	}
	
	/**
	 * @param string|null $contenu 
	 * @return self
	 */
	public function setContenu(?string $contenu): self {
		$this->contenu = $contenu;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getNombreDeVue(): ?int {
		return $this->nombreDeVue;
	}
	
	/**
	 * @param int|null $nombreDeVue 
	 * @return self
	 */
	public function setNombreDeVue(?int $nombreDeVue): self {
		$this->nombreDeVue = $nombreDeVue;
		return $this;
	}
}