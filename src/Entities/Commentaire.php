<?php

namespace App\Entities;
use DateTime;

class Commentaire {
    private ?int $id;
    private ?string $user;
    private ?string $contenu;
    private ?DateTime $date;
    private ?string $email;
    private ?int $idArticles;

    /**
     * @param int|null $id
     * @param string|null $user
     * @param string|null $contenu
     * @param DateTime|null $date
     * @param string|null $email
     * @param int|null $idArticles
     */
    public function __construct(?string $user, ?string $contenu, ?DateTime $date, ?string $email, ?int $idArticles, ?int $id =null) {
    	$this->id = $id;
    	$this->user = $user;
    	$this->contenu = $contenu;
    	$this->date = $date;
    	$this->email = $email;
    	$this->idArticles = $idArticles;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getUser(): ?string {
		return $this->user;
	}
	
	/**
	 * @param string|null $user 
	 * @return self
	 */
	public function setUser(?string $user): self {
		$this->user = $user;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContenu(): ?string {
		return $this->contenu;
	}
	
	/**
	 * @param string|null $contenu 
	 * @return self
	 */
	public function setContenu(?string $contenu): self {
		$this->contenu = $contenu;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getEmail(): ?string {
		return $this->email;
	}
	
	/**
	 * @param string|null $email 
	 * @return self
	 */
	public function setEmail(?string $email): self {
		$this->email = $email;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getIdArticles(): ?int {
		return $this->idArticles;
	}
	
	/**
	 * @param int|null $idArticles 
	 * @return self
	 */
	public function setIdArticles(?int $idArticles): self {
		$this->idArticles = $idArticles;
		return $this;
	}
}